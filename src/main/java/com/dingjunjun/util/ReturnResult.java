package com.dingjunjun.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**前端返回封装的信息
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReturnResult {

    private boolean flag;

    private Integer code;

    private String message;

    private Object data;
}
