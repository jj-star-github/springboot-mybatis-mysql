package com.dingjunjun.util;

import java.io.Serializable;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */

public class PageQuery implements Serializable {

    private static final long serialVersionUID = -6610138130146415533L;

    private  int start;

    private  int totalCount;

    private int end;

    private  int pageSize;
    private  int pageNum;

    public int getTotalCount() {
        return totalCount;
    }

    public int getEnd() {
        return pageSize;
    }


    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getStart() {

        return (pageNum-1)*pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageIndex(int pageIndex) {
        this.pageNum = pageIndex;
    }

    public void setStart(int start) {
        this.start = start;
    }
    public PageQuery(){
        this.start  = 0;
        this.end = 3;
        this.pageNum = 1;
        this.pageSize = 3;
    }
}
