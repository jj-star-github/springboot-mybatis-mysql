package com.dingjunjun.controller;

import com.dingjunjun.pojo.Account;
import com.dingjunjun.pojo.dto.QueryInfoDto;
import com.dingjunjun.service.AccountService;
import com.dingjunjun.util.ReturnResult;
import com.dingjunjun.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 22372 @Description: @Version: V1.0
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    /**
     * 查询所有
     *
     * @return
     */
    @GetMapping("/accountFindAll")
    public ReturnResult accountFindAll() {
        return new ReturnResult(true, StatusCode.code, "查询所有成功！", accountService.findAll());
    }


    /**
     * 分页查询
     *
     * @return
     */
    @GetMapping("/pageQueryInfo")
    public ReturnResult pageQueryInfo(@RequestBody QueryInfoDto queryInfoDto) {
        return new ReturnResult(true, StatusCode.code, "分页查询成功！", accountService.pageQueryInfo(queryInfoDto));
    }


    /**
     * 根据id查询
     *
     * @return
     */
    @GetMapping("/findAccountById")
    public ReturnResult findAccountById(@RequestBody QueryInfoDto queryInfoDto) {
        return new ReturnResult(true, StatusCode.code, "根据id查询成功！", accountService.findAccountById(queryInfoDto));
    }


    /**
     * 添加单个account
     *
     * @return
     */
    @PostMapping("/addAccount")
    public ReturnResult addAccount(@RequestBody Account account) {
        return new ReturnResult(true, StatusCode.code, "添加单个account成功！", accountService.addAccount(account));
    }


    /**
     * 批量添加account
     *
     * @return
     */
    @PostMapping("/batchAddAccount")
    public ReturnResult batchAddAccount(@RequestBody List<Account> accountList) {
        return new ReturnResult(true, StatusCode.code, "批量添加account成功！", accountService.batchAddAccount(accountList));
    }


    /**
     * 根据id批量删除
     *
     * @param idList
     * @return
     */
    @DeleteMapping("/batchDeleteAccount/{idList}")
    public ReturnResult batchDeleteAccount(@PathVariable List<Integer> idList) {
        return new ReturnResult(true, StatusCode.code, "批量删除account成功！", accountService.batchDeleteAccount(idList));
    }
}
