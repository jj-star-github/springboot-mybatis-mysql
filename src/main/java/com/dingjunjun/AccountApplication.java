package com.dingjunjun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@SpringBootApplication
@MapperScan(basePackages = "com.dingjunjun.mapper")
public class AccountApplication {
  public static void main(String[] args) {
      SpringApplication.run(AccountApplication.class,args);
  }
}
