package com.dingjunjun.mapper;

import com.dingjunjun.pojo.Account;
import com.dingjunjun.pojo.dto.QueryInfoDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
public interface AccountMapper {

    /**
     * 查询所有
     *
     * @return
     */
    List<Account> findAll();

    /**分页查询
     * @param queryInfoDto
     * @return
     */
    List<Account> pageQueryInfo(@Param("queryInfoDto") QueryInfoDto queryInfoDto,@Param("pageNum") int pageNum,@Param("pageSize") int pageSize);


    /**统计个数
     *
     * @return
     */
    int count();

    /**根据id查询
     *
     * @param queryInfoDto
     * @return
     */
    Account findAccountById(@Param("queryInfoDto")QueryInfoDto queryInfoDto);

    /**添加单个account
     *
     * @param account
     */
    void addAccount(Account account);

    /**批量添加account
     *
     * @param accountList
     */
    void batchAddAccount(@Param("accountList") List<Account> accountList);

    /**根据id批量删除
     *
     * @param idList
     * @return
     */
    boolean batchDeleteAccount(@Param("idList") List<Integer> idList);
}
