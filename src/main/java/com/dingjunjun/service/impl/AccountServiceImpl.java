package com.dingjunjun.service.impl;

import com.alibaba.fastjson.JSON;
import com.dingjunjun.mapper.AccountMapper;
import com.dingjunjun.pojo.Account;
import com.dingjunjun.pojo.dto.QueryInfoDto;
import com.dingjunjun.service.AccountService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    private AccountMapper accountMapper;

    /**
     * 查询所有
     *
     * @return
     */
    @Override
    public List<Account> findAll() {
        LOGGER.info("into method findAll");
        return accountMapper.findAll();
    }

    /**
     * 指定分页查询
     *
     * @return
     */
    @Override
    public PageInfo<Account> pageQueryInfo(QueryInfoDto queryInfoDto) {
        LOGGER.info("into method pageQueryInfo，入参：" + JSON.toJSONString(queryInfoDto));
        PageHelper.startPage(queryInfoDto.getPageNum(), queryInfoDto.getPageSize());
        List<Account> accountList = accountMapper.pageQueryInfo(queryInfoDto, queryInfoDto.getPageNum(), queryInfoDto.getPageSize());
        PageInfo<Account> accountPageInfo = new PageInfo<>(accountList);
        accountPageInfo.setPageNum(queryInfoDto.getPageNum());
        return accountPageInfo;
    }

    /**
     * 统计个数
     *
     * @return
     */
    @Override
    public int count() {
        return accountMapper.count();
    }

    /**
     * 根据id查询
     *
     * @param queryInfoDto
     * @return
     */
    @Override
    public Account findAccountById(QueryInfoDto queryInfoDto) {
        LOGGER.info("into method findAccountById,入参：" + JSON.toJSONString(queryInfoDto));
        return accountMapper.findAccountById(queryInfoDto);
    }

    /**
     * 添加单个account
     *
     * @param account
     * @return
     */
    @Override
    public boolean addAccount(Account account) {
        LOGGER.info("into method addAccount,入参：" + JSON.toJSONString(account));
        if (null == account) {
            log.info("account不能为空！");
        } else {
            accountMapper.addAccount(account);
        }
        return true;
    }

    /**
     * 批量添加account
     * //todo 没有控制住往里面添加null值: 问题已经解决
     *
     * @param accountList
     * @return
     */
    @Override
    public boolean batchAddAccount(List<Account> accountList) {
        LOGGER.info("into method batchAddAccount,入参：" + JSON.toJSONString(accountList));
        if (!StringUtils.isEmpty(accountList)) {
            for (Account account : accountList) {
                if (null != account && null != account.getName() && account.getMoney() > 0) {
                    accountMapper.batchAddAccount(accountList);
                } else if (null == account.getName() || account.getMoney() > 0) {
                    log.error("输入的account值不合法！");
                    throw new RuntimeException("输入的account值不合法！");
                } else if (null != account.getName() || account.getMoney() < 0) {
                    log.error("输入的account值不合法！");
                    throw new RuntimeException("输入的account值不合法！");
                }
            }
        }
        return true;
    }

    /**
     * 根据id批量删除
     *
     * @param idList
     * @return
     */
    @Override
    public boolean batchDeleteAccount(List<Integer> idList) {
        LOGGER.info("into method batchDeleteAccount,入参：" + JSON.toJSONString(idList));
        if (!StringUtils.isEmpty(idList)) {
            for (Integer id : idList) {
                if (null != id) {
                    accountMapper.batchDeleteAccount(idList);
                }
            }
        }
        return true;
    }
}
