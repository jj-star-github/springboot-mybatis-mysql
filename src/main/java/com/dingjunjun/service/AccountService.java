package com.dingjunjun.service;

import com.dingjunjun.pojo.Account;
import com.dingjunjun.pojo.dto.QueryInfoDto;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
public interface AccountService {

    /**
     * 查询所有
     * @return
     */
    List<Account> findAll();


    /**分页查询
     *
     * @return
     */
    PageInfo<Account> pageQueryInfo(QueryInfoDto queryInfoDto);


    /**统计个数
     *
     * @return
     */
    int count();


    /**根据id查询
     *
     * @param queryInfoDto
     * @return
     */
    Account findAccountById(QueryInfoDto queryInfoDto);

    /**添加单个account
     *
     * @param account
     * @return
     */
    boolean addAccount(Account account);

    /**批量添加account
     *
     * @param accountList
     * @return
     */
    boolean batchAddAccount(List<Account> accountList);

    /**根据id批量删除
     *
     * @param idList
     * @return
     */
    boolean batchDeleteAccount(List<Integer> idList);
}
