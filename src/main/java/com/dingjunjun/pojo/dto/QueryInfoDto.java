package com.dingjunjun.pojo.dto;

import com.dingjunjun.util.PageQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**封装查询的各个条件
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryInfoDto extends PageQuery implements Serializable {
    /**
     * 根据姓名模糊查询
     */
    private String name;

    /**
     * 根据id查询
     */
    private Integer id;

    //===============其他条件任意封装=======================//

}
